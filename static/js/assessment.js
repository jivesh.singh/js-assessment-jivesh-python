/*

Javascript Machine Test Questions :


 1)- Write the Javascript function for the following:

     var data$ = [3,47,8,9,345,907,5,5,8,4,345]

     - Number should be greater than 4 & without duplicate entries.
     - Addition of all the elements in the array.
     - Output should be print on <P> tag.


2)- Write the Javascript function for the following:

    var numberX = [2,3,56,34,83,7,5,6,9]
    var dataX = [4,56,3,4,78,94,7,5,6]

    - Concat the both arrays without duplicate entries.
    - if the number is prime, move it to another array.
    - Output should be two arrays : one with prime number & another is without prime number.


3)- Write the javascript function to print the Fibonacci series & Output of all the values in Object.

4)- Write the javascript function to sort the array without using any javascript function.

    var sort$ = [4,96,34,5,65,203,87,345] 

*/



// Solutions

var resultDiv= document.createElement("div");
resultDiv.setAttribute("id","result");
document.body.appendChild(resultDiv);



// Solution #1
function solution1(dataArr){
    var uniqueArr= [];
    var result= [];

    // Filtering out Duplicate values and numbers less than 4
    for (var i = 0; i < dataArr.length; i++) {
        var flag = false;

        if(dataArr[i]>4){
            for (var j = 0; j < uniqueArr.length; j++) {
            
                if (dataArr[i] == uniqueArr[j]) {
    
                    flag = true;
                    break;
                }
    
            }
    
            if (!flag) {
                uniqueArr[uniqueArr.length] = dataArr[i];
            }
        }

        else{
            continue;
        }


    }

    // Adding all the elements in the unique array
    var sum=0;

    for(var k=0;k<uniqueArr.length;k++){
        sum+=uniqueArr[k];
        
    }

    result[0]= uniqueArr;
    result[1]= sum
    return result;
}

var data$ = [3,47,8,9,345,907,5,5,8,4,345]
var sol1= solution1(data$);

// Output
var sol1Para= document.createElement('p');
sol1Para.setAttribute("id","solution-1");
sol1Para.innerHTML= "<h3>Solution of Ques #1</h3><br>Output Array: ["+ sol1[0]+"]<br>Sum is: "+sol1[1];
document.getElementById("result").appendChild(sol1Para);





// Solution #2

function solution2(numArr, dataArr){
    var resArr= [];

    for(var i=0;i<numArr.length;i++){

        resArr[resArr.length]= numArr[i];
    }

    for(var j=0; j<dataArr.length;j++){

        var flag = false;

    
        for (var l = 0; l < resArr.length; l++) {
        
            if (dataArr[j] == resArr[l]) {

                flag = true;
                break;
            }

        }

        if (!flag) {
            resArr[resArr.length] = dataArr[j];
        }

    }

    
    //Separate Prime no.s and Non-prime no.s
    var primeArr=[];
    var nonPrimeArr=[];
    for(var a=0; a<resArr.length;a++){
        if(resArr[a] === 1){
            nonPrimeArr[nonPrimeArr.length]= resArr[a];
        }
        
        else{
            var prime= true;
            for(var x=2;x<resArr[a];x++){
                {
                    if(resArr[a]%x === 0){
                        prime= false;
                        break;
                    }
                    else{
                        continue;
                    }
                }
            }

            if(prime){
                primeArr[primeArr.length]= resArr[a];
            }
            else{
                nonPrimeArr[nonPrimeArr.length]= resArr[a];
            }
            
        }  
    }

    var sol2_Obj= {};
    sol2_Obj['Concat Array']= resArr;
    sol2_Obj['Prime Array']= primeArr;
    sol2_Obj['Non-prime Array']= nonPrimeArr;

    return sol2_Obj;
    
}



var numberX = [2,3,56,34,83,7,5,6,9];
var dataX = [4,56,3,4,78,94,7,5,6];

var sol2= solution2(numberX,dataX);

// Output
var sol2Para= document.createElement('p');
sol2Para.setAttribute("id","solution-2");
sol2Para.innerHTML= "<br><h3>Solution of Ques #2</h3><br>Resulting Array after Concatenation is: ["+sol2['Concat Array']+"]<br>Array of Prime Numbers: ["+sol2['Prime Array']+"]<br>Array of Non-prime numbers: ["+sol2['Non-prime Array']+"]";
document.getElementById("result").appendChild(sol2Para);




// Solution #3 (To print the Fibonacci series & Output of all the values in Object.)


// Here n is the size of Fibonacci series.
function fibonacci(n){

    // Fibonacci Series
    var first= 0;
    var second= 1;
    var result=[];
    result[0]= first;
    result[1]= second;

    for(var i=2; i<n;i++){
        var num= first+second;
        result[result.length]= num;
        first= second;
        second= num;
    }


    // Storing Fibonacci Series in an Object
    var obj={};
    for(var j=0; j<result.length;j++){
        obj[j]= result[j];
    }


    return obj;
}

var fiboSize=10;
var sol3= fibonacci(fiboSize);
console.log(sol3);

// Output
var sol3Para1= document.createElement('p');
sol3Para1.setAttribute("id","solution-3-1");
sol3Para1.innerHTML= "<br><h3>Solution of Ques #3</h3><br>Fibonacci series containing "+fiboSize+" elements: ";
document.getElementById("result").appendChild(sol3Para1);


for(var z=0;z<fiboSize;z++){
    var sol3Para2= document.createElement('p');
    var paraID= "solution-3-2-"+z;
    sol3Para2.setAttribute("id",paraID);
    sol3Para2.innerHTML= z+" : "+sol3[z];
    document.getElementById("result").appendChild(sol3Para2);

}






// Solution #4 (To sort the array without using any javascript function.)

function sortArray(arr){
    var sortedArr = [];
    for (var i = 0; i < arr.length; i++) {

        for (var k = 0; k < arr.length; k++) {

            if (arr[k] > arr[k + 1]) {
                var temp = arr[k];
                arr[k] = arr[k + 1];
                arr[k + 1] = temp;
            }
            else {
                continue;
            }
        }
    }

    for (var z = 0; z < arr.length; z++) {
        sortedArr[sortedArr.length]= arr[z];
    }

    return sortedArr;
}

var sort$ = [4,96,34,5,65,203,87,345];
var sol4= sortArray(sort$);

// Output
var sol4Para= document.createElement('p');
sol4Para.setAttribute("id","solution-4");
sol4Para.innerHTML= "<br><h3>Solution of Ques #4</h3><br>Sorted Array is: ["+sol4+"]";
document.getElementById("result").appendChild(sol4Para);
